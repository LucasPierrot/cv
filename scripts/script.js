document.onreadystatechange = () => {

    if (document.readyState === "interactive") {

        const TEXTES_LOADING = document.querySelectorAll(':is(h1,h2,h3,span,p,button)')

        TEXTES_LOADING.forEach((texte) => {
            texte.classList.add('loading')
        });

    }
    if (document.readyState === "complete") {
        appInit();
        console.log('Complete stage')
    }
};


function appInit() {

    const TEXTES_LOADING = document.querySelectorAll(':is(h1,h2,h3,span,p,button)')
    var CAN_SCROLL = true
    const SLIDE_SWITCH_DURATION = 750
    var IS_DRAGGING = false;
    var START_POSITION = 0;


    TEXTES_LOADING.forEach((texte) => {
        texte.classList.remove('loading')
        texte.classList.add('loaded')
    });

    // -------------- EventListener pour le click de navigation vers la slide suivante --------------
    var nbrSections = document.querySelectorAll('section').length;

    for (let index = 1; index < nbrSections; index++) {
        document.querySelector('#slide' + index + ' button.slide_suivante').addEventListener("click", function () { ClickSwitchSlide((index + 1)) })
    }

    function ClickSwitchSlide(nbr) {
        document.querySelector('main').scroll(document.getElementById('slide' + nbr).offsetLeft, 0)
    }

    // -------------- EventListener pour le scroll à la souris --------------

    document.querySelector('main').addEventListener('wheel', (event) => {
        if (CAN_SCROLL) {
            let hasOpenAttribute = [...DIALOGS].some(dialog => dialog.hasAttribute('open'));
            if (hasOpenAttribute) {
                return
            }
            event.preventDefault();
            CAN_SCROLL = false;
            document.querySelector('main').scrollBy({
                left: event.deltaY < 0 ? -1 : 1,
            });
            setTimeout(() => {
                CAN_SCROLL = true
            }, SLIDE_SWITCH_DURATION);
        }
    });

    // -------------- EventListener pour le scroll avec les flèches directionnelles --------------

    window.addEventListener("keydown", function (e) {
        if (["ArrowLeft", "ArrowRight"].indexOf(e.code) > -1) {
            e.preventDefault();
            let hasOpenAttribute = [...DIALOGS].some(dialog => dialog.hasAttribute('open'));
            if (hasOpenAttribute) {
                return
            }
        }

        if ("ArrowLeft".indexOf(e.code) > -1) {
            if (CAN_SCROLL) {
                CAN_SCROLL = false;
                document.querySelector('main').scrollBy({
                    left: -1,
                });
                setTimeout(() => {
                    CAN_SCROLL = true
                }, SLIDE_SWITCH_DURATION);
            }
        }

        if ("ArrowRight".indexOf(e.code) > -1) {
            if (CAN_SCROLL) {
                CAN_SCROLL = false;
                document.querySelector('main').scrollBy({
                    left: 1,
                });
                setTimeout(() => {
                    CAN_SCROLL = true
                }, SLIDE_SWITCH_DURATION);
            }
        }
    }, false);

    // -------------- EventListener pour le scroll en drag & drop --------------


    document.querySelector('main').addEventListener('mousedown', (e) => {
        IS_DRAGGING = true;
        START_POSITION = e.pageX - document.querySelector('main').offsetLeft;
        document.querySelector('main').style.cursor = 'grab';
    });

    document.querySelector('main').addEventListener('mouseleave', () => {
        IS_DRAGGING = false;
        document.querySelector('main').style.cursor = 'auto';
    });

    document.querySelector('main').addEventListener('mouseup', () => {
        IS_DRAGGING = false;
        document.querySelector('main').style.cursor = 'auto';
    });

    document.querySelector('main').addEventListener('mousemove', (e) => {
        if (!IS_DRAGGING) return;
        e.preventDefault();
        if (CAN_SCROLL && (START_POSITION - (e.pageX - document.querySelector('main').offsetLeft) < -100)) {
            document.querySelector('main').scrollBy({
                left: -1
            })
            CAN_SCROLL = false
            setTimeout(() => {
                CAN_SCROLL = true
            }, SLIDE_SWITCH_DURATION);
        } else if (CAN_SCROLL && (START_POSITION - (e.pageX - document.querySelector('main').offsetLeft) > 100)) {
            document.querySelector('main').scrollBy({
                left: 1
            })
            CAN_SCROLL = false
            setTimeout(() => {
                CAN_SCROLL = true
            }, SLIDE_SWITCH_DURATION);
        }
    });

    // -------------- Gestion de l'observer --------------

    const slides = document.querySelectorAll('.slide')

    const observer = new IntersectionObserver(entries => {
        entries.forEach(entry => {
            entry.target.classList.toggle('displayed', entry.isIntersecting)

            // Pour conserver le statut "displayed" indéfiniment
            // if (entry.isIntersecting) observer.unobserve(entry.target)
        })
    }, {
        threshold: .95
    })

    slides.forEach(slide => {
        observer.observe(slide)
    })

    // -------------- Gestion des <dialog> --------------

    const DIALOGS = document.querySelectorAll('dialog')

    const SLIDE2_DIALOGS = document.querySelectorAll('#slide2 dialog')
    const ETUDES = document.querySelectorAll('#slide2 .etudes')
    const SLIDE3_DIALOGS = document.querySelectorAll('#slide3 dialog')
    const PROJETS = document.querySelectorAll('#slide3 .projets a')
    const SLIDE4_DIALOGS = document.querySelectorAll('#slide4 dialog')
    const EXPERIENCES = document.querySelectorAll('#slide4 .experience')
    const SLIDE5_DIALOGS = document.querySelectorAll('#slide5 dialog')
    const COMPETENCES = document.querySelectorAll('#slide5 .competence')


    ETUDES.forEach((etude, index) => {
        etude.addEventListener('click', () => {
            SLIDE2_DIALOGS[index].showModal();
            SLIDE2_DIALOGS[index].classList.add('fade')
        });
    });

    PROJETS.forEach((projet, index) => {
        projet.addEventListener('click', () => {
            SLIDE3_DIALOGS[index].showModal();
            SLIDE3_DIALOGS[index].classList.add('fade')
        });
        projet.addEventListener("keypress", function (event) {
            if (event.key === "Enter") {
                SLIDE3_DIALOGS[index].showModal();
                SLIDE3_DIALOGS[index].classList.add('fade')
            }
        });
    });

    EXPERIENCES.forEach((experience, index) => {
        experience.addEventListener('click', () => {
            SLIDE4_DIALOGS[index].showModal();
            SLIDE4_DIALOGS[index].classList.add('fade')
        });
        experience.addEventListener("keypress", function (event) {
            if (event.key === "Enter") {
                SLIDE4_DIALOGS[index].showModal();
                SLIDE4_DIALOGS[index].classList.add('fade')
            }
        });
    });

    COMPETENCES.forEach((competence, index) => {
        competence.addEventListener('click', () => {
            SLIDE5_DIALOGS[index].showModal();
            SLIDE5_DIALOGS[index].classList.add('fade')
        });
        competence.addEventListener("keypress", function (event) {
            if (event.key === "Enter") {
                SLIDE5_DIALOGS[index].showModal();
                SLIDE5_DIALOGS[index].classList.add('fade')
            }
        });
    });

    DIALOGS.forEach((dialog) => {
        dialog.addEventListener("click", e => {
            const dialogDimensions = dialog.getBoundingClientRect()
            if (
                e.clientX < dialogDimensions.left ||
                e.clientX > dialogDimensions.right ||
                e.clientY < dialogDimensions.top ||
                e.clientY > dialogDimensions.bottom
            ) {
                setTimeout(() => {
                    dialog.classList.remove('fade')
                    dialog.close()
                }, 0);
            }
        })
    })

    // -------------- Gestion des réalisations --------------

    const projects = document.querySelector('#slide3 .projects')

    projects.addEventListener('mousemove', (e) => {
        CAN_SCROLL = false
    });

    projects.addEventListener('mouseleave', (e) => {
        CAN_SCROLL = true
    });

    // -------------- Gestion du scroll form --------------

    const form = document.querySelector('#slide6 .part:nth-child(1)')

    form.addEventListener('mousemove', (e) => {
        CAN_SCROLL = false
    });

    form.addEventListener('mouseleave', (e) => {
        CAN_SCROLL = true
    });
}